module gitlab.com/stud777/stuff

go 1.17

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/goombaio/namegenerator v0.0.0-20181006234301-989e774b106e // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rs/zerolog v1.25.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	gitlab.com/di5connect/golang v1.0.1 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
