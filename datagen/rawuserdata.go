package datagen

var (
	emptyStrVal = ""
)

// RawUserData contatins user data taken from row like
// "Ганнибал Андрей Преподов    realemail@mail.com  non-generated-password  prep"
type RawUserData struct {
	Name    string
	Midname string
	Surname string
	email   *string

	passwd *string

	// isProfessor true if row contains `prep`
	IsProfessor bool
}

func (ud *RawUserData) Email() string {
	if ud.email != nil {
		return *ud.email
	}

	return emptyStrVal
}

// Passwd desired password
func (ud *RawUserData) Passwd() string {
	if ud.passwd != nil {
		return *ud.passwd
	}

	return emptyStrVal
}

func (ud *RawUserData) SetEmail(s string) {
	ud.email = &s
}

func (ud *RawUserData) SetPasswd(s string) {
	ud.passwd = &s
}
