package datagen

import (
	"math/rand"
	"strings"
	"time"

	"github.com/goombaio/namegenerator"
)

const (
	NULL = "NULL"
)

type RawUserDataGenerator interface {
	GetRawUserDataString() (string, error)
}

type rawMoodleUserDataGenerator struct {
}

func NewRawMusGen() RawUserDataGenerator {
	return &rawMoodleUserDataGenerator{}
}

// GetUserDataString return user data string - surname name midname (email@mail.vom, NULL) (non-generated-password, NULL)  [prep].
// (e.g. Иванов Андрей Васильевич realemail@mail.com  some-password  prep)
func (m rawMoodleUserDataGenerator) GetRawUserDataString() (string, error) {

	seed := time.Now().UTC().UnixNano()
	nameGenerator := namegenerator.NewNameGenerator(seed)
	str1 := nameGenerator.Generate()
	str2 := nameGenerator.Generate()
	mailDomainNames := nameGenerator.Generate()

	names := []string{}
	nms := strings.Split(str1, "-")
	names = append(names, nms...)

	nms = strings.Split(str2, "-")
	names = append(names, nms...)

	mailDomain := strings.Replace(mailDomainNames, "-", ".", 1)

	prep := ""
	if rand.Intn(100) > 90 {
		prep = "prep"
	}

	nonGeneratedPassword := NULL
	if rand.Intn(100) > 50 {
		nonGeneratedPassword = StringWithCharset(7)
	}

	// result := names[0] + " " + names[1] + " " + names[2] + " " + getMailOrNULL(names[3], mailDomain+".com ") + " " + nonGeneratedPassword + " " + prep
	result := GenerateFIO() + " " + getMailOrNULL(names[3], mailDomain+".com ") + " " + nonGeneratedPassword + " " + prep

	return result, nil
}

func getMailOrNULL(username string, mailDomain string) string {
	if rand.Intn(100) > 50 {
		return username + "@" + mailDomain
	}
	return NULL
}

// StringWithCharset generate random string.
func StringWithCharset(length int) string {

	const charset = "abcdefghijklmnopqrstuvwxyz" +
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	var seededRand *rand.Rand = rand.New(
		rand.NewSource(time.Now().UnixNano()))

	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}
