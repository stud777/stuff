# Some stuff

## How-to

To get module use:

```golang
    go get -u gitlab.com/stud777/stuff
    go mod tidy
```

```golang
    import "gitlab.com/stud777/stuff/datagen"

   	gen := datagen.RawUserDataGenerator{}

	for i := 0; i < 100; i++ {
		usStr, err := gen.GetRawUserDataString()
		if err != nil {
			panic(err)
		}
		println(usStr)
	}

```
