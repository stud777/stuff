package test

import (
	"gitlab.com/stud777/stuff/datagen"

	"testing"
)

func Test_GetUserString(t *testing.T) {

	gen := datagen.NewRawMusGen()

	for i := 0; i < 100; i++ {
		usStr, err := gen.GetRawUserDataString()
		if err != nil {
			panic(err)
		}
		println(usStr)
	}

}
